from fastapi import FastAPI, File, UploadFile
from fastapi.staticfiles import  StaticFiles
from starlette.responses import HTMLResponse

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")

@app.get("/")
@app.get("/home")
@app.get("/me")
@app.get("/post_01")
@app.get("/post_02")
def  blog_view():
    f = open("static/index.html", "r")
    s = f.read()
    f.close()
    return HTMLResponse(s)

@app.get("/get_post")
def  get_post(num):
    f = open(f"static/posts/post_{num}.md", "r")
    content = f.read()
    f.close()
    return {"content": content}
