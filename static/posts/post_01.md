# CREATE A BLOG USING FASTAPI, REACT, DOCKER AND AWS

###### Last update: 2021-03-19

Among lockdown and partial unemployment, in 2020 I started to develop some projects about data science and software engineering. I thought it would be nice to expose them. I could have created a Medium page or something like that but why not develop my own site, exactly like I want it to be ? Furthermore, as data scientist, building web app isn't my prediclection skill, so it is a great exercice.

For the back-end, I chose to use FastApi because I never really used it and I knew that it is a really simple and powerfull framework. For the front-end, React has been an obvious choice for me, because I already knew it and it allow to build beautiful web app.

For the deployment, because my dev ops skills are quite limited, I chose what I thought was the more common and not too complex: dockerized the app and use AWS platform for hosting.

Let's developed and deployed a website from scratch :smile: !

### Setting up our FastApi backend

First thing to do is to create a virtual env dedicated to this project. I like to store all my venvs at the same place on my machine, so I have a venv-dir in my home.

```bash
$ python3 -m venv ~/venv-dir/myblog-env
```

Then activate it and install fastapi and uvicorn.

```bash
$ source ~/venv-dir/myblog-env/bin/activate
$ pip install fastapi uvicorn
```

Then set up our project directory.

```bash
$ mkdir ~/myblog
$ cd ~/myblog
$ touch main.py
```

Let's create our FastAPI app. In `main.py` we will declare our FastAPI instance and add simple routes.

```python
# main.py
from  fastapi  import  FastAPI, File, UploadFile

app = FastAPI()

@app.get("/")
def blog_home_page():
	return {"Hello": "World"}

@app.get("/get_data")
def  get_data():
	return {"data": ["toto", "tata", "titi"]}
```

We can test that our app is working by running

```bash
$ uvicorn main:app --reload
```

and go to [localhost:8000](http://localhost:8000/) . You should see that your app is running :tada: ! You can also try [localhost:8000/docs](http://localhost:8000/docs) which is your API auto-generated documentation.

### Integrate a React front-end

Now that your back-end is ready :sunglasses:, let's setting up the front-end !

First set up our front-end projet using [Create React App](https://create-react-app.dev/).

```bash
$ npx create-react-app frontend && cd frontend
```

You can customize default home page by editing `/frontend/src/App.js`:

```javascript
import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>This will be my blog home page !</p>
      </header>
    </div>
  );
}

export default App;
```

then you can test your React app in development mode by running:

```bash
$ yarn start
```

and go to [localhost:3000](http://localhost:3000/). Your front-end is up :fire: !

By using `yarn start`, you only launch your front-end, there is no communication with your back-end which btw can be shutdown. For futur developments it will be necessary to communicate from front-end to back-end and vice versa.

To serve this purpose, we will try to fetch data from back-end to front-end. Do you remember the route `get_data` that we've declared in our `main.py` ? It return a list and we want to display it in your front home page.

First thing to do is to start again your back-end. So open a new terminal and run the following at our project root (don't forget to reactivate your venv).

```bash
$ uvicorn main:app
```

Then you have to add a proxy which will redirect front-end development mode queries to our back-end. So in `frontend/package.json` add the following:

```json
"proxy": "http://localhost:8000",
```

To fetch data, we will use **axios** package, which is a promise based HTTP client. Go back in your front-end terminal, and add this package to our node modules:

```bash
$ yarn add axios
```

Relaunch your front end with `yarn start`.

In `/frontend/src/App.js` you should now me able to import axis module, so add the following:

```javascript
import axios from "axios";
```

and check everything is ok on [localhost:3000](http://localhost:3000/).

To fetch data from back-end we will use axios associated to the `useState` and `useEffect` React Hooks. In order to organize our projet we will also create a sub component.

So create a directory `/frontend/src/components/` and a file `/frontend/src/components/HomePage.js`.

```bash
$ mkdir ~/myblog/frontend/src/components
$ touch ~/myblog/frontend/src/components/HomePage.js
```

Modify your `/frontend/src/App.js` to:

```javascript
import "./App.css";
import { HomePage } from "./components/HomePage";

const App = () => {
  return <HomePage />;
};
export default App;
```

Then add the following in `/frontend/src/components/HomePage.js`:

```javascript
import React, { useState, useEffect } from "react";
import axios from "axios";
import logo from "./../logo.svg";

export const HomePage = () => {
  const [backendData, setBackendData] = useState({});

  const fetch_backend_data = () => {
    const headers = {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    };
    axios
      .get("/get_data", { headers: headers })
      .then((resp) => {
        setBackendData(resp.data);
      })
      .catch((err) => {
        console.log("Error", err);
      });
  };

  useEffect(() => {
    fetch_backend_data();
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>This will be my blog home page !</p>
        <br />
        <p>
          Data from back-end: <br />
          <br />[ {backendData.data
            ? backendData.data.map((v) => {
                return " " + v + " ";
              })
            : ""} ]
        </p>
      </header>
    </div>
  );
};
```

Then if you relaunched your front (`yarn start`) and get back on [localhost:3000](http://localhost:3000/) you should be able to see the data fetch from back-end are displayed ! :boom:

Now we would like to access to our front-end through [localhost:8000](http://localhost:8000/), as production must do.

To achieve this, our front must be build. Run the following from your front-end root directory:

```bash
$ yarn build
```

This will generate a `build` folder containing all your front-end static files.

Now how to serve static files to our FastAPI app ? It can be done using `aiofiles` python package which allow you to serve static files automatically from a directory.

So go back to your project root directory and create a `static` folder:

```bash
$ mkdir ~/myblog/static
```

Then install `aiofiles`

```bash
$ pip install aiofiles
```

Then update `main.py` to mount your static folder:

```python
# main.py
from  fastapi  import  FastAPI, File, UploadFile
from  fastapi.staticfiles  import  StaticFiles

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")

@app.get("/")
def  blog_home_page():
	return {"Hello": "World"}

@app.get("/get_data")
def  get_data():
	return {"data": ["toto", "tata", "titi"]}
```

Then transfer all front static files in this static directory:

```
$ cp -r ~/myblog/frontend/build/static/*  ~/myblog/static/
$ cp ~/myblog/frontend/build/index.html  ~/myblog/static/
```

Finally update your blog home route to rendex static `index.html`

```python
# main.py
from  fastapi  import  FastAPI, File, UploadFile
from  fastapi.staticfiles  import  StaticFiles
from starlette.responses import HTMLResponse

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")

@app.get("/")
def  blog_home_page():
    f = open("static/index.html", "r")
    s = f.read()
    f.close()
    return HTMLResponse(s)

@app.get("/get_data")
def  get_data():
	return {"data": ["toto", "tata", "titi"]}
```

This is it ! Launch your app with `uvicorn main:app` and go to [localhost:8000](http://localhost:8000/) to see your home page directly through FastAPI !

### Dockerized the application

**todo**

- pip freeze requirements.txt
- Dockerfile
- docker build
- docker run

**text**

We just developed and tested our application locally, this is great but we're not going to stop here: let's dockerized our application.

Here I suppose you already have Docker installed on your machine, if not follow [these](https://www.docker.com/get-started) instructions.

Why should we use docker ?

- It will run on every environment (say good bye to the popular _But it worked on my machine !_)
- System ressources will be used in a more efficient way
- Deployment is easier
- ..

First step is to log your projects requirements:

```bash
$ pip freeze > ~/myblog/requirements.txt
```

Then create a Dockerfile at your projet root directory.

```bash
$ touch ~/myblog/Dockerfile
```

A Dockerfile is a text document that contains all the commands a user could call on the command line to assemble an image. Fill it with the following:

```
FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

WORKDIR /app
COPY . /app

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

EXPOSE 8000
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
```

Here you can notice the command line `EXPOSE 8000` which inform Docker that the container will listen on port 8000 at runtime. Indeed the next command: `CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]` start our app on port 8000.

Then build and tag the image by running:

```bash
$ docker build -t myblog -f ~/myblog
```

And finally run it with:

```bash
$ docker run -dp 5000:8000 myblog
```

Visit [localhost:5000](http://localhost:5000): your app is running dockerized !

### Deploy the application

**todo**

- AWS account
- EC2 instance
  - get pem and connect
  - setting up instance
    - install docker
    - install AWS CLI
    - ... ?
  - security group
  - ... ?
- ECR
  - create registry
  - get push command
- Local
  - AWS CLi installation / configuration
  - Push docker images to ECR

Add schéma ?

**text**

Now that you have a dockerized application, let's deploy it through AWS platform. We will use two distinct AWS services: ECR and EC2.

First step: create a AWS account. So let's go to [AWS](https://aws.amazon.com/) and sign up.

**Create IAM Group and account**

> AWS Identity and Access Management (IAM) is a web service that helps you securely control access to AWS resources. You use IAM to control who is authenticated (signed in) and authorized (has permissions) to use resources.

To access to your AWS ressources through command lines, you need and AIM user with sufficient rights.

Let's go to [IAM groups page](https://console.aws.amazon.com/iam/home?region=eu-west-3#/groups) and click on "Create New Group".

1. Let's call it _admin-group_ and click on Next Step

2. Give him full authorization by checking _AdministratorAccess_ and click again on Next Step

3. Click on _Create group_

Then go to [IAM user page](https://console.aws.amazon.com/iam/home?region=eu-west-3#/users) and click on "Add user"

1. Call it _admin-user_ and let's give him programmatic access and AWS Management Console access types. Alson choose a custom password for it. Click on Next: Permissions

![aws01](/static/posts_img/p01_createuser.png)

2. Select Add user to group and select admin-group previously created. Click on Next: Tag

![aws01](/static/posts_img/p01_selectgroup.png)

3. Click on Next: Review and create user

4. Download pair keys of your user. Keep it sake !

![aws01](/static/posts_img/p01_download_keypair.png)

**Push your docker image to ECR**

> Amazon Elastic Container Registry (ECR) is a fully managed container registry that makes it easy to store, manage, share, and deploy your container images and artifacts anywhere.

First thing you have to do is to install AWS client on your machine, to be able to push docker image to ECR (more info [here](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html)).

```bash
$ curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
$ unzip awscliv2.zip
$ sudo ./aws/install
```

Now let's configure our AWS client. In your terminal type

```bash
aws configure
```

and enter the AIM access keys previously downloaded.

Then go to [ECR](https://eu-west-3.console.aws.amazon.com/ecr/home) home and create a new repository.

![aws01](/static/posts_img/p01_create_ecr_repo.png)

Default settings are ok for us, just add a repository name, here _myblog_.

Once this is done, open your repo and click on "View push commands".

Copy, past and run the four proposed command lines, which will respectivelly:

- Retrieve an authentication token and authenticate your Docker client to your registry
- Build your Docker image (we already have done this but there is no problem to do it again)
- Tag your image so you can push the image to your ECR repository
- Push this image to your newly created AWS repository

**Set up an EC2 instance**

> Amazon Elastic Compute Cloud (Amazon EC2) provides scalable computing capacity in the Amazon Web Services (AWS) Cloud. Using Amazon EC2 eliminates your need to invest in hardware up front, so you can develop and deploy applications faster.

Go to [EC2](https://eu-west-3.console.aws.amazon.com/ec2/v2/home), click on "Launch instances" then

1. Select Ubuntu server 18
2. Select t2 micro and click on "Review and Lauch" and "Launch" again.
3. It is then ask for a key pair. Select Create a new key pair, name it _myblog-key_, download it and finnaly click on launch instances.

Your instance in now running, but with default parameters, you can only access it via ssh. We need to allow trafic from web (HTTP). So go on your instance in the EC2 dashboard, click on it and then select "Security " tab. Click on the Security groups link. In the Inbound Rules table, click on Edit inbound rules. Add the two following rules:

img !!!

Everything is now ready to deploy your app on your instance. So let's connect to it.

Grab your instance Public IPv4 DNS and connect to it through SSH.

```bash
chomd 400 /path/to/your/instance/key-pair.pem
ssh -i /path/to/your/instance/key-pair.pem ubuntu@ec2-15-236-179-6.eu-west-3.compute.amazonaws.com
```

In this demonstration we will use default ubuntu user, but this is no a

On your instance, install docker (follow [this page](https://docs.docker.com/engine/install/ubuntu/) instructions). Don't forget to add ubuntu user in the docker group

```bash
sudo usermod -aG docker ubuntu
```

Then install AWS Cli and configure it as we did earlier locally and configure it.

Authenticate your Docker client to your registry

```bash
aws ecr get-login-password --region eu-west-3 | docker login --username AWS --password-stdin 042331064152.dkr.ecr.eu-west-3.amazonaws.com
```

Then grab your image URI and pull it

```bash
docker pull 042331064152.dkr.ecr.eu-west-3.amazonaws.com/myblog:latest
```

run it

```bash
docker run -p 80:8000 042331064152.dkr.ecr.eu-west-3.amazonaws.com/myblog
```
