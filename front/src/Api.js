import axios from "axios";

export function get_post_content(num, setFunc) {
  const headers = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  };
  const queryUrl = "/get_post?num=" + num;
  axios
    .get(queryUrl, { headers: headers })
    .then((resp) => {
      setFunc(resp.data.content);
    })
    .catch((err) => {
      console.log("Error", err);
    });
}
