import React from "react";
import { Timeline, Typography } from "antd";
import { StudyHatIcon } from "./../icons/StudyHatIcon.js";
import { LaptopIcon } from "./../icons/LaptopIcon.js";

const { Title } = Typography;

export const WorkTimeLine = () => {
  return (
    <>
      <Title style={{ textAlign: "center" }}>Work Experiences</Title>

      <Timeline mode={"alternate"}>
        <Timeline.Item
          dot={<LaptopIcon width={30} height={30} color={"#00b3ff"} />}
        >
          <div className="timeline-content">
            <div className="timeline-title">Data Scientist @Fieldbox.ai</div>
            <div className="timeline-date">September 2018 - Now</div>
            <div>
              Development and deployment of machine learning algorithms and
              expert applications for main industry actors
            </div>
          </div>
        </Timeline.Item>

        <Timeline.Item
          dot={<LaptopIcon width={30} height={30} color={"#00b3ff"} />}
        >
          <div className="timeline-content">
            <div className="timeline-title">Data Scientist Intern @NP6</div>
            <div className="timeline-date">February 2018 - September 2018</div>
            <div>
              Development of a recommendation engine for the NP6 CM platform
            </div>
          </div>
        </Timeline.Item>

        <Timeline.Item
          dot={<LaptopIcon width={30} height={30} color={"#00b3ff"} />}
        >
          <div className="timeline-content">
            <div className="timeline-title">
              Statistical Developer Intern @Addinsoft
            </div>
            <div className="timeline-date">May 2017 - August 2017</div>
            <div>
              Development of the XLSTAT software Support Vector Machine module
            </div>
          </div>
        </Timeline.Item>

        <Timeline.Item
          dot={<LaptopIcon width={30} height={30} color={"#00b3ff"} />}
        >
          <div className="timeline-content">
            <div className="timeline-title">Data analyst Intern @DREAL</div>
            <div className="timeline-date">April 2014 - June 2014</div>
            <div>
              Publication of a regional energy diagnostic in the industry
              sector.
            </div>
          </div>
        </Timeline.Item>
      </Timeline>
    </>
  );
};

export const EducationTimeLine = () => {
  return (
    <>
      <Title style={{ textAlign: "center" }}>Education</Title>

      <Timeline mode={"alternate"}>
        <Timeline.Item
          dot={<StudyHatIcon width={30} height={30} color={"#7400b8"} />}
        >
          <div className="timeline-content">
            <div className="timeline-title">
              Master of Sciences in Mathematics, Statistics and Computer Science
            </div>
            <div className="timeline-date" style={{ color: "#7400b8" }}>
              2016 - 2018
            </div>
          </div>
        </Timeline.Item>
        <Timeline.Item
          dot={<StudyHatIcon width={30} height={30} color={"#7400b8"} />}
        >
          <div className="timeline-content">
            <div className="timeline-title">
              Bachelor degree in Mathematics, Economics and Computer Science
            </div>
            <div className="timeline-date" style={{ color: "#7400b8" }}>
              2014 - 2016
            </div>
          </div>
        </Timeline.Item>
        <Timeline.Item
          dot={<StudyHatIcon width={30} height={30} color={"#7400b8"} />}
        >
          <div className="timeline-content">
            <div className="timeline-title">
              University degree in Statistics, Technology and Business
              Intelligence
            </div>
            <div className="timeline-date" style={{ color: "#7400b8" }}>
              2012 - 2014
            </div>
          </div>
        </Timeline.Item>
      </Timeline>
    </>
  );
};
