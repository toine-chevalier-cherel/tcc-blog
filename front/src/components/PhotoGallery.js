import React from "react";

import Gallery from "react-photo-gallery";
import { Typography } from "antd";

const { Title } = Typography;

export const PhotoGallery = () => {
  const photos = [
    {
      src: "static/photos/p01.jpg",
      width: 2,
      height: 2.66,
    },
    {
      src: "static/photos/p02.jpg",
      width: 2.66,
      height: 2,
    },
    {
      src: "static/photos/p03.jpg",
      width: 2,
      height: 2.66,
    },
    {
      src: "static/photos/p04.jpg",
      width: 2.66,
      height: 2,
    },
    {
      src: "static/photos/p05.jpg",
      width: 1,
      height: 1,
    },
    {
      src: "static/photos/p06.jpg",
      width: 2.66,
      height: 2,
    },
    {
      src: "static/photos/p07.jpg",
      width: 1,
      height: 1,
    },
  ];

  return (
    <>
      <Title style={{ textAlign: "center" }}>Sneak peek of my life</Title>
      <Gallery photos={photos} />;
    </>
  );
};
