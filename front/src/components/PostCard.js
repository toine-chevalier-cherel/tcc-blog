import React from "react";

import { Link } from "react-router-dom";
import { isMobile } from "react-device-detect";

export const PostCard = ({ img, link, title, text, words }) => {
  return (
    <>
      <Link to={link}>
        <div className={isMobile ? "post-div-mobile" : "post-div"}>
          <div>
            <img src={img} className="post-img" />
            <div className="post-title-div">{title}</div>
            <div className="post-text-div">{text}</div>
          </div>
          <div className="key-word-div">
            {words.map((word) => (
              <li>{word}</li>
            ))}
          </div>
        </div>
      </Link>
    </>
  );
};
