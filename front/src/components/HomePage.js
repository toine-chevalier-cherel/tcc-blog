import React from "react";

import { Divider } from "antd";
import { PostCard } from "./PostCard.js";

import post01Img from "../images/post_01_blog_img.jpg";
import post02Img from "../images/post_02_dart_img.jpg";
import { isMobile } from "react-device-detect";

export const HomePage = () => {


  const PostCard01 = () => {
    return (
      <PostCard
        img={post01Img}
        link={"/post_01"}
        title={
          "Develop and deploy a blog from scratch"
        }
        text={
          "A step by step guide to develop a FastAPI app communicating with a React front-end, and to deploy it on AWS. This is the origin story of this blog."
        }
        words={["FastAPI", "React", "Docker", "AWS"]}
      />
    )
  }

  const PostCard02 = () => {
    return (
      <PostCard
        img={post02Img}
        link={"/post_02"}
        title={"Develop and deploy a dart app"}
        text={"This is the description: Bryan is in the kitchen."}
        words={["Django", "React", "AWS"]}
      />
    )
  }

  return (
    <div className={isMobile ? "main-content-mobile" : "main-content"}>
      <div className="intro-div-code">{'print("Welcome!")'}</div>

      <Divider style={{ margin: "32px 0px", backgroundColor: "#c2c2c2" }} />

      <div className="intro-div-text">
        Hi and welcome on my blog ! I'm Toine, a full stack Data
        Scientist, passionate about Data Science and all its applications. Also
        I enjoy software development which is really useful to expose Data
        Science results. On this website you will find articles about some
        humble personnal projects realized during my lockdown free time.
      </div>

      <Divider style={{ margin: "32px 0px", backgroundColor: "#c2c2c2" }} />

      <div className="posts-div">
        <div className="posts-raw-div">
          <PostCard01 />
          {/* <PostCard02 />
          <PostCard01 /> */}
        </div>
        {/* <div className="posts-raw-div">
          <PostCard01 />
          <PostCard02 />
          <PostCard01 />
        </div> */}
      </div>
    </div>
  );
};
