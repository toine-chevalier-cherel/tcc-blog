import React from "react";
import { Layout } from "antd";
import { HeartTwoTone } from "@ant-design/icons";

const { Footer } = Layout;

export const AppFooter = () => {
  return (
    <Footer className="footer-div">
      <div style={{ marginRight: "10px" }}> Made with </div>
      <HeartTwoTone twoToneColor="#ff0000" />
      <div style={{ marginLeft: "10px" }}>
        {" "}
        in Bordeaux by Toine Chevalier-Cherel.{" "} <div style={{fontSize: "12px"}}>Last update: 2021-03-15</div>
      </div>
    </Footer>
  );
};
