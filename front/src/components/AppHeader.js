import React from "react";
import { Layout, Button, message } from "antd";
import { Link } from "react-router-dom";
import Icon from "@ant-design/icons";
import linkedInImg from "../images/linkedin.png";
import gitLabImg from "../images/gitlab.png";
import gmailImg from "../images/gmail.png";
import { isMobile } from "react-device-detect";
import { NetworkLogoIcon } from "../icons/NetworkLogoIcon.js"
const { Header } = Layout;

export const AppHeader = () => {
  return (
    <Header className="header-div">
      <div className="header-bloc-div">
        <div style={{display: "flex", flexDirection: "column", justifyContent: "space-around"}}>
          <NetworkLogoIcon width={50} height={50}/>
        </div>
        <Link to={"/home"} style={{display: "flex"}}>
          <div
            className={
              isMobile ? "header-element-mobile" : "header-element"
            }
          >
            Toine Chevalier - Data Science
          </div>
        </Link>
      </div>

      <div className="header-bloc-div">
        <div className="header-element" style={{ padding: "2px 0px" }}>
          <Link to={"/home"}>
            <div 
            className={
              isMobile ? "header-button header-button-mobile" : "header-button"
            }
            >
              Home
            </div>
          </Link>
        </div>

        <div className="header-element" style={{ padding: "2px 0px" }}>
          <Link to={"/me"}>
          <div
          className={
              isMobile ? "header-button header-button-mobile" : "header-button"
            }
            >
              About me
            </div>
          </Link>
        </div>

        <div className="header-element">
          <a
            target="_blank"
            href="https://gitlab.com/users/toine.chevalier.cherel/projects"
          >
            <Button
              icon={
                <Icon component={() => <img src={gitLabImg} height={28} />} />
              }
              style={{
                border: "none",
              }}
            ></Button>
          </a>
        </div>

        <div className="header-element">
          <a>
            <Button
              icon={
                <Icon component={() => <img src={gmailImg} height={26} />} />
              }
              style={{
                border: "none",
              }}
              onClick={() => {
                navigator.clipboard.writeText(
                  "toine.chevalier.cherel@gmail.com"
                );
                message.success("My email has been copied to your clipboard !");
              }}
            ></Button>
          </a>
        </div>

        <div className="header-element">
          <a target="_blank" href="https://www.linkedin.com/in/toinecc/">
            <Button
              icon={
                <Icon component={() => <img src={linkedInImg} height={28} />} />
              }
              style={{
                border: "none",
              }}
            ></Button>
          </a>
        </div>
      </div>
    </Header>
  );
};
