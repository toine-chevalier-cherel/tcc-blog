import React from "react";

import ReactMarkdown from "react-markdown";
import emoji from "emoji-dictionary";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { dracula } from "react-syntax-highlighter/dist/esm/styles/prism";

const emojiSupport = (text) =>
  text.value.replace(/:\w+:/gi, (name) => emoji.getUnicode(name));

const renderers = {
  text: emojiSupport,
  code: ({ language, value }) => {
    return (
      <SyntaxHighlighter style={dracula} language={language} children={value} />
    );
  },
};

export const PostPage = ({ markdown }) => {
  return (
    <div className="main-content">
      <div className="markdown-div">
        <ReactMarkdown
          source={markdown}
          plugins={[emoji]}
          renderers={renderers}
        />
      </div>
    </div>
  );
};
