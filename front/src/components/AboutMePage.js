import React, { useState } from "react";

import { PhotoGallery } from "./PhotoGallery.js";
import { EducationTimeLine, WorkTimeLine } from "./Timelines.js";
import { Divider } from "antd";

export const AboutMePage = () => {
  return (
    <div className="main-content">
      <WorkTimeLine />
      <Divider />
      <EducationTimeLine />
      <Divider />
      <PhotoGallery />
    </div>
  );
};
