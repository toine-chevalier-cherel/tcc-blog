import "./App.less";
import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";

import { Layout } from "antd";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

import { HomePage } from "./components/HomePage.js";
import { AppHeader } from "./components/AppHeader.js";
import { AppFooter } from "./components/AppFooter.js";
import { PostPage } from "./components/PostPage.js";
import { get_post_content } from "./Api.js";
import { AboutMePage } from "./components/AboutMePage";

const { Content } = Layout;

const App = () => {
  const [post01, setPost01] = useState("");
  const [post02, setPost02] = useState("");

  const get_posts_content = () => {
    get_post_content("01", setPost01);
    get_post_content("02", setPost02);
  };

  useEffect(() => {
    get_posts_content();
  }, []);

  return (
    <Router>
      <Layout className="app-css">
        <AppHeader />
        <Content>
          <Switch>
            <Route path={"/"}>
              <Switch>
                <Route exact path={"/"}>
                  <Redirect to={"/home"} />
                </Route>
                <Route path={"/home"}>
                  <HomePage />
                </Route>
                <Route path={"/me"}>
                  <AboutMePage />
                </Route>
                <Route path={"/post_01"}>
                  <PostPage markdown={post01} />
                </Route>
                <Route path={"/post_02"}>
                  <PostPage markdown={post02} />
                </Route>
              </Switch>
            </Route>
          </Switch>
        </Content>
        <AppFooter />
      </Layout>
    </Router>
  );
};

export default App;
